package comparable;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company com1, Company com2) {
		if (com1.getExpense() < com2.getExpense()){
			return -1;
		}
		else if (com1.getExpense() > com2.getExpense()){
			return 1;
		}
		
		return 0;
	}
	

}
