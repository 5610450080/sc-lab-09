package comparable;

import java.util.Comparator;

public class TaxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable tax1, Taxable tax2) {
		if (tax1.getTax() < tax2.getTax()){
			return -1;
		}
		else if (tax1.getTax() > tax2.getTax()){
			return 1;
		}
		return 0;
	}
	

}
