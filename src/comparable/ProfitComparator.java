package comparable;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company com1, Company com2) {
		if (com1.getProfit() < com2.getProfit()){
			return -1;
		}
		else if (com1.getProfit() > com2.getProfit()){
			return 1;
		}
		return 0;
	}
	

}
