package comparable;

public class Product implements Taxable,Comparable<Product>{
	private String name;
	private int price;
	
	public Product(String name,int price){
		this.name = name;
		this.price = price;
	}
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}
	public int compareTo(Product o){
		if (price < o.price){
			return -1;
		}
		else if (price > o.price){
			return 1;
		}
		return 0;
	}
	public String toString(){
		return "Name : "+name+" Price : "+price+" Tax : "+getTax();
	}
	@Override
	public double getTax() {
		double tax = 0.7*price;
		return tax;
	}

}
