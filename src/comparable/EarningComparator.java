package comparable;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{


	@Override
	public int compare(Company com1, Company com2) {
		 if(com1.getEarning() <  com2.getEarning()){
			 return -1;
		 }
		 else if(com1.getEarning() > com2.getEarning()) {
			 return 1;
		}
	    return 0;
	}
}
	


