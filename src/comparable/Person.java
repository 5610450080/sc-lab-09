package comparable;

public class Person implements Taxable,Comparable<Person>{
	private String name;
	private int height;
	private int saraly;
	private double tax;
	
	public Person(String name,int saraly,int height){
		this.name = name;
		this.saraly = saraly;
		this.height = height;
	}
	public String getName(){
		return name;
	}
	public double getHeight(){
		return height;
	}
	public double getSalary(){
		return saraly;
	}
	public String toString(){
		return "Name : " +name+" Height : "+height+" Salary : "+saraly+" Tax : "+getTax();
	}
	@Override
	public int compareTo(Person o) {
		if (saraly < o.saraly){
			return -1;
		}
		else if(saraly > o.saraly){
			return 1;
		}
		
		return 0;
	}
	@Override
	public double getTax() {
		if (saraly == 300000){
			tax =  0.05*300000;
			return tax ;
			
		}
		else if (saraly > 300000){
			int s = saraly - 300000;
			tax = 0.05*300000 + 0.1*s;
			return tax;
		}
		return 0;
	}
	

}
