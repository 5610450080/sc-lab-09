package comparable;

import java.awt.List;
import java.util.*;

public class TestCase {
	public static void main(String[] args){
		ArrayList<Person> ar = new ArrayList<Person>();
		ar.add(new Person("Ploy",350000,158));
		ar.add(new Person("Mos",300000,178));
		ar.add(new Person("Toey",320000,180));
		Collections.sort(ar);
		System.out.println(ar);
		
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("cookie",50));
		product.add(new Product("chocolate",40));
		product.add(new Product("cake",100));
		Collections.sort(product);
		System.out.println(product);
		
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Ploy",30000,5000));
		company.add(new Company("Mos",50000,2000));
		company.add(new Company("Toey",7000,1000));
		Collections.sort(company,new EarningComparator());
		Collections.sort(company,new ExpenseComparator());
		Collections.sort(company,new ProfitComparator());
		System.out.println(company);
		
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		Person per = new Person("Ploy",3500000,158);
		Product pro = new Product("Cake",100);
		Company com = new Company("Mos",30000,5000);
		tax.add(per);
		tax.add(pro);
		tax.add(com);
		Collections.sort(tax,new TaxComparator());
		System.out.println(tax);
		
		
	}

}
