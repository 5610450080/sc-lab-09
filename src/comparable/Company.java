package comparable;

public class Company implements Taxable{
	private String name; 
	private int earning;
	private int expense,profit;
	
	public Company(String name,int earning,int expense){
		this.earning = earning;
		this.expense = expense;
		this.name = name;;
	}
	public String getName(){
		return name;
	}
	public int getEarning(){
		return earning;
	}
	public int getExpense(){
		return expense;
	}
	public int getProfit(){
		profit = earning - expense;
		return profit;
	}
	public String toString(){
		return "Name : "+name+" Earning : "+earning+" Expense : "+expense+" Profit : "+profit+" Tax : "+getTax(); 
	}
	@Override
	public double getTax() {
		double profit = earning - expense;
		double tax = 0.3*profit;
		return tax;
	}

}
